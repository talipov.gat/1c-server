  �'      ResB             x P   �   �  �         �  IDS_USAGE_WIN IDS_USAGE_LINUX IDS_UNKNOWN_PARAMETER IDS_IIS_NOTFOUND IDS_PATH_REQUIRED IDS_NAME_REQUIRED IDS_CONNSTR_REQUIRED IDS_CONFPATH_REQUIRED IDS_INCOMP_KEYS IDS_WEBSRV_REQUIRED IDS_WSDIR_OR_DESCRIPTOR_REQUIRED IDS_DIR_KEY_REQUIRED IDS_CONNSTR_OR_DESCRIPTOR_KEY_REQUIRED IDS_PUB_NOTFOUND IDS_WRONG_CONNECTIONSTRING IDS_WRONG_DIRECTORY IDS_WRONG_DIRECTORY1 IDS_DELETE_SUCCESS IDS_PUB_SUCCESS IDS_PUB_UPDATED IDS_OBSOLETTE_KEY IDS_EXCEPTION IDS_WWWROOT_DENIED IDS_WEBINST_NOADAPTER IDS_DESCRIPTOR_REQUIRED IDS_DESCRIPTORNOTFOUND IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_WINDOWS IDS_ADMIN_PRIVILEGES_REQUIRED_WARNING_LINUX �  8:;NG5==O:     C1;V:0FVO  28:>=0=0  C1;V:0FVO  >=>2;5=0  C1;V:0FVO  =5  7=0945=0  5A:@8?B>@  =5  7=0945=89  ;NGV  % s   V  % s   =5AC<VA=V.   51- A5@25@  I I S   =5  7=0945=89.   w e b s r v   -   >1>2 O7:>289  ?0@0<5B@  V4:;NG5==O  ?C1;V:0FVW  28:>=0=5  ;NG  % s   70AB0@V2,   28:>@8AB>2C9B5  % s   52V4><89  ?0@0<5B@  :><0=4=>3>  @O4:0:     +�- d i r   -   >1>2 O7:>289  ?0@0<5B@  ?@8  ?C1;V:0FVW  ,�0B0;>3  % s   =5  <>65  1CB8  :0B0;>3><  ?C1;V:0FVW  -�5>1EV4=>  2:070B8  :;NG  - w s d i r   01>  - d e s c r i p t o r   /�5>1EV4=>  2:070B8  :;NG  - c o n n s t r   01>  - d e s c r i p t o r   /�5>1EV4=>  2:070B8  @O4>:  7 T4=0==O  2  ?0@0<5B@V:     /�5>1EV4=>  2:070B8  V< O  ?C1;V:0FVW  2  ?0@0<5B@V:     2�5>1EV4=>  2:070B8  H;OE  4>  . v r d   D09;C  2  ?0@0<5B@V:     2�5>1EV4=>  2:070B8  :0B0;>3  ?C1;V:0FVW  2  ?0@0<5B@V:     7�0B0;>3  45A:@8?B>@0  =5  A?V2?040T  7  :0B0;>3><  ?C1;V:0FVW  9�5>1EV4=>  2:070B8  H;OE  4>  c o n f   D09;C  A p a c h e   2  ?0@0<5B@V:     :�0B0;>3  ?0@0<5B@0  - d i r   =5  A?V2?040T  7  :0B0;>3><  ?C1;V:0FVW  N� O4>:  7 T4=0==O  ?0@0<5B@0  - c o n n s t r   =5  A?V2?040T  V7  @O4:><  7 T4=0==O  ?C1;V:0FVW  s�5  2AB0=>2;5=V  <>4C;V  @>7H8@5==O  251- A5@25@0. 
 ;O  28:>=0==O  ?C1;V:0FVW  =5>1EV4=>  7<V=8B8  CAB0=>2:C  1 !: V4?@8T<AB20.   ��;O  28:>=0==O  FVTW  >?5@0FVW  ?>B@V1=V  ?>2=>2065==O  AC?5@:>@8ABC20G0  ( r o o t ) . 
 =0:H5  <>6;820  =5:>@5:B=0  @>1>B0  ?@>3@0<8. 
 ;O  70?CA:C  2  @568<V  AC?5@:>@8ABC20G0  28:>=09B5  :><0=4C  G5@57  s u d o .   �;O  28:>=0==O  FVTW  >?5@0FVW  ?>B@V1=V  ?>2=>2065==O  04<V=VAB@0B>@0  !. 
 =0:H5  <>6;820  =5:>@5:B=0  @>1>B0  ?@>3@0<8. 
 ;O  70?CA:C  V7  ?>2=>2065==O<8  04<V=VAB@0B>@0  =5>1EV4=>  70?CAB8B8  :><0=4=89  @O4>:  2  @568<V  04<V=VAB@0B>@0,   0  ?>BV<  28:>=0B8  :><0=4C  2  FL><C  :><0=4=><C  @O4:C.   ���1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   �ߣ1 C : @54?@8OB85  8 .   #B8;8B0  ?C1;8:0F88  251- :;85=B0 
  
         C1;8:0F8O:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   - w s d i r   V i r t u a l D i r   - d i r   D i r   - c o n n s t r   c o n n S t r   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         C1;8:0F8O  =0  >A=>25  ACI5AB2CNI53>  v r d   D09;0:  
  
         w e b i n s t   [ - p u b l i s h ]   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   - d i r   D i r   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]   [ - o s a u t h ]  
  
         #40;5=85  ?C1;8:0F88:  
  
         w e b i n s t   - d e l e t e   w e b s r v   - w s d i r   V i r t u a l D i r   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         #40;5=85  ?C1;8:0F88  ?>  ACI5AB2CNI5<C  v r d   D09;C:  
  
         w e b i n s t   - d e l e t e   w e b s r v   [ - w s d i r   V i r t u a l D i r ]   - d e s c r i p t o r   v r d P a t h   [ - d i r   D i r ]   [ - c o n n s t r   c o n n S t r ]   [ - c o n f P a t h   c o n f P a t h ]  
  
         ;NG8:  
  
                 - p u b l i s h :   >?C1;8:>20BL,   :;NG  ?>  C<>;G0=8N 
                 - d e l e t e :   C40;8BL  ?C1;8:0F8N 
                 w e b s r v  
                         - i i s :   ?C1;8:0F8O  51- :;85=B0  4;O  I I S  
                         - a p a c h e 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 0  
                         - a p a c h e 2 2 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 2  
                         - a p a c h e 2 4 :   ?C1;8:0F8O  51- :;85=B0  4;O  A p a c h e   2 . 4  
                 - w s d i r   V i r t u a l D i r :   28@BC0;L=K9  :0B0;>3 
                 - d i r   D i r :   D878G5A:89  :0B0;>3,   2  :>B>@K9  1C45B  >B>1@065=  28@BC0;L=K9 
                 - d e s c r i p t o r   v r d P a t h :   ?CBL  :  ACI5AB2CNI5<C  v r d   D09;C 
                 - c o n n s t r   c o n n S t r :   AB@>:0  A>548=5=8O   
                 - c o n f P a t h   c o n f P a t h :   ?>;=K9  ?CBL  :  :>=D83C@0F8>==><C  D09;C  A p a c h e   ( B>;L:>  4;O  ?C1;8:0F88  =0  A p a c h e )  
                 - o s a u t h :   8A?>;L7>20=85  W i n d o w s   02B>@870F88  ( B>;L:>  4;O  ?C1;8:0F88  =0  I I S )    c5� � �� �T � w �e 5��> .   �� Fau� ������ M A$ ~ e � u6  " � ��
�� Y�Q��