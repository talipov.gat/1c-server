  �'      ResB             � P   �	  !  !  e      !  IDS_CMD_DISTRIBUTIVE IDS_CMD_DISTRIBUTIVESETUP IDS_UPDATEFILENAMETITLE IDS_CMD_CREATEDISTRIBUTIVEANDUPDATE IDS_PARENTSUPPORTFREE IDS_PARENTSUPPORTWARNING IDS_PARENTSUPPORTPROTECT IDS_PRVEXTNEEDSAVE IDS_PRVEXTNEEDBUILD IDS_PRVEXTNEEDPROVIDER IDS_PRVEXTNEEDRELEASE IDS_CONFIG_FILE_TYPE_NAME IDS_PRVEXTNEEDVALIDNAME IDS_PRVEXTREPLACEFILE IDS_CATEGORY_DEVDEPOT IDS_CMD_CREATE_DEVDEPOT IDS_BAND_DEVDEPOT IDS_CMD_BIND_DEVDEPOT IDS_CMD_UNBIND_DEVDEPOT IDS_CMD_GET_DEVOBJECT IDS_CMD_REVISE_DEVOBJECT IDS_CMD_ENROLL_DEVOBJECT IDS_CMD_CANCEL_REVISING_DEVOBJECT IDS_DEV_DEPOT_CREATE_COMMENT IDS_CMD_REFRESH_DEVDEPOT_INFO IDS_CMD_CONFIGEXTFILEINFO IDS_FILEMODE IDS_SELFMODE IDS_NAME IDS_RELEASE IDS_PROVIDER IDS_VERSION IDS_STOREMODE IDS_CONFIGINFOEQUAL IDS_MISSING_OBJECT_ENTRY IDS_CREATEDISTRIBUTIVESTATUS IDS_DEPOT_PATH_CHOOSE IDS_ERR_DEPOT_NOTFOUND IDS_ERR_DEPOT_CANT_CHANGE_DB_ACCESS IDS_ERR_DEPOT_BINDING_ANOTHER_USER IDS_ERR_DEPOT_USER_DUPLICATE_NAME_REMOVED_USER IDS_QUEST_DUP_NAME_RESURRECT_USER IDS_ERR_DEPOT_SHARING_VIOLATION IDS_ERR_DEPOT_DB_ALREADY_EXIST IDS_ERR_DEPOT_ALREADY_AUTHENTICATED IDS_ERR_DEPOT_NOT_FOUND IDS_ERR_DEPOT_CREATE_FAULT IDS_ERR_DEPOT_NOT_BINDED IDS_ERR_DEPOT_NOT_AUTHENTICATED IDS_ERR_DEPOT_OPEN_FAULT IDS_ERR_DEPOT_CLEAR_LAST_ADMIN_RIGHTS IDS_ERR_DEPOT_ADD_USER_FAULT IDS_ERR_DEPOT_EDIT_USER_FAULT IDS_ERR_DEPOT_REMOVE_USER_FAULT IDS_ERR_DEPOT_RESURRECT_USER_FAULT IDS_ERR_DEPOT_UNKNOWN_DEPOT_ID IDS_CMD_DEVDEPOT_HISTORY IDS_CMD_DEVOBJECT_HISTORY IDS_CMD_UPDATE_CONFIG_FROM_DEPOT IDS_DEPOT_BIND_STRING_MISMATCH IDS_ERR_DEPOT_CHANGE_PASSWORD_FAULT IDS_DEPOT_QUEST_CONVERT IDS_ERR_DEPOT_GET_OBJECT_FAULT IDS_DEPOT_ADMIN_NAME IDS_ERR_DEPOT_AUTHENTICATION_FAULT IDS_CMD_DEVDEPOT_ADMIN IDS_ERR_DEPOT_USER_NOT_FOUND IDS_DEPOT_SHARING_VIOLATION IDS_ERR_DEPOT_BINDING_FAULT IDS_ERR_DEPOT_OBJECT_NOT_FOUND IDS_ERR_DEPOT_OBJECT_REVISE_FAULT IDS_ERR_DEPOT_OBJECT_ENROLL_FAULT IDS_ERR_DEPOT_FAILURE IDS_CMD_OPEN_DEVDEPOT_VIEW IDS_CONFIRM_PASSWORD_FAULT IDS_DEPOT_GET_OBJECTS_DLG_CAPTION IDS_DEPOT_REVISE_OBJECTS_DLG_CAPTION IDS_DEPOT_CANCEL_REVISING_OBJECTS_DLG_CAPTION IDS_ERR_DEPOT_BINDING_STRING_MISMATCH IDS_DEPOT_ENROLL_OBJECTS_DLG_CAPTION IDS_BAND_DEVDEPOT_TOOLBAR IDS_ERR_DEPOT_INCOMPATIBLE_VERSION IDS_ERR_DEPOT_DELETE_ONLINE_USER IDS_UNREVISED_OBJECT IDS_REMOVED_OBJECT IDS_UPDATED_OBJECT IDS_REVISED_OBJECT IDS_ERR_DEPOT_USER_DUPLICATE_NAME IDS_ERR_DEPOT_USER_DUPLICATE_ID IDS_UNPROCESSED_OBJECT IDS_ERR_DEPOT_BINDING_ALREADY_BINDED_USER IDS_ERR_DEPOT_USER_NAME_NOT_FOUND IDS_ERR_DEPOT_CANT_RESET_ADMIN_RIGHTS IDS_CONFIRM_DELETE_DEPOT_USER IDS_ERR_DEPOT_USER_PASSWORD_MISMATCH IDS_ERR_DEPOT_DELETE_LAST_USER IDS_DEPOT_COLLECT_DEV_INFO IDS_DEPOT_WRITE_DEV_INFO IDS_DEPOT_CREATE_STRUCTURE IDS_DEPOT_MAKE_SNAPSHOT_LIST IDS_CREATE_DEPOT_URL_QUEST IDS_DEPOT_UNABLE_COMPLETE_OPERATION IDS_ERR_DEPOT_OLD_VERSION IDS_DEPOT_BIND_ALREADY_BINDED_USER IDS_DEPOT_BEGIN_OPERATION IDS_DEPOT_END_OPERATION IDS_DEPOT_CANCEL_OPERATION IDS_REVISE_OBJECT IDS_UNREVISE_OBJECT IDS_ENROLLED_OBJECT IDS_CMD_SAVE_DEVDEPOT_SNAPSHOT IDS_CMD_COMPARE_DEVDEPOT_SNAPSHOT IDS_STATUS_DEPOT_DEVOBJECTS_GET_REQUEST IDS_UPDATECONFIG_FILE_TYPE_NAME IDS_FILEALREADYEXISTSINLIST IDS_UPDATETHISVERSIONERROR IDS_SRCCONFIGIMPOSSIBLE IDS_SRCCONFIGNOTDISTRIBUTIVE IDS_ERR_COUNTSRCCONFIGS IDS_ERR_INVALIDFILENAME IDS_UNCHANGED_OBJECT IDS_CMD_DEVDEPOT_VIEW_FILTER IDS_BAND_DEVDEPOT_VIEW IDS_ERR_DEPOT_CANT_CANCEL_REVISE_OBJECTS IDS_INFO_CLEAN_REMOVED_REFS_1 IDS_ERR_DEPOT_REVISE_ANOTHER_USER_REVISED_OBJS IDS_CMD_COMPARE_DEVDEPOT_SNAPSHOT_WITH_FILE IDS_WARN_DEPOT_CANT_CANCEL_REVISE_OBJECTS IDS_QUEST_DEPOT_GET_REVISED_CHECK IDS_DEPOT_ENROLL_ERR_REASON1 IDS_DEPOT_ENROLL_ERR_REASON2 IDS_CONFIGUPDATE_FILE_TYPE_NAME IDS_CONFIGFILETITLE IDS_QUEST_DEPOT_CANCEL_REVISING IDS_DEPOT_NOT_UNDER_CONTROL_OBJECT IDS_DEPOT_REVISED_ANOTHER_USER_OBJECT IDS_DEPOT_NOT_REVISED_OBJECT IDS_DEPOT_REVISED_OBJECT IDS_CMD_ADDFROMPREVIOUSRELEASES IDS_DEVDEPOT_VIEW_TYPE_TREE IDS_DEVDEPOT_VIEW_TYPE_LIST IDS_CMD_DEVDEPOT_VIEW_CLEAR_FILTER IDS_CREATEUPDATEFILE IDS_REQUIRED_DEV_OBJECTS_FOUND IDS_DEV_DEPOT_BIND_INFO_MISSING IDS_DEPOT_REVISED_PARENT_OBJECT IDS_DEPOT_NOT_REVISED_PARENT_OBJECT IDS_DEPOT_REVISED_ANOTHER_USER_PARENT_OBJECT IDS_REMOVED_REFERENCES_DEV_OBJECTS_FOUND IDS_DISTRIBUTIVEFILENAMETITLE IDS_CREATEDISTRIBUTIVEANDUPDATEOK IDS_CANCEL_REVISING_CONFIRMAION IDS_FILENAME IDS_ERR_DEPOT_OPERATION_NOT_ALLOWED IDS_ERR_DEPOT_CANT_UNBIND_ONLINE_USER IDS_ERR_DEPOT_ENROLL_REQUIRED_OBJS_MISSING IDS_WARN_DEPOT_ENROLL_REQUIRED_OBJS_MISSING IDS_CLOSEDCONFIG IDS_PRVEXTNEEDSAVE1 IDS_PRVEXTNEEDBUILD1 IDS_BADPARAMETERS IDS_RULE IDS_OBJECT IDS_CMD_PROVIDERMODESELECT IDS_CMD_GET_DEV_DEPOT_VERSION IDS_ERR_REVISED_OBJECTS_FOUND IDS_WARN_CHANGE_CONFIG_FROM_DEPOT IDS_CMD_DEVDEPOT_ROLLBACK_VERSION IDS_CMD_DEVDEPOT_CUT_OLD_VERSION IDS_QUEST_CHANGE_CONFIG IDS_DEVDEPOTCNFTITLE IDS_DEVDEPOTONLY IDS_MERGEFILTERSHOWDEVDEPOT IDS_ORDERDEVDEPOT IDS_ORDER_DEVDEPOT IDS_MERGERULE_DEVDEPOT IDS_MERGERULE_MERGEDEVDEPOT IDS_BADUPDATEURL IDS_CMD_DEVDEPOT_MERGE_VERSIONS IDS_REPOBJECTDEVDEPOT IDS_CHECKSUBSYSTEMDEVDEPOT IDS_CMD_CREATE_DEVDEPOT_VERSION_LABEL IDS_ERR_DEPOT_OPERATION_FAILED IDS_DEVDEPOT_VERSION_TITLE IDS_DEVDEPOT_ROLLBACK_VERSIONS_CONFIRMATION IDS_DEVDEPOT_MERGE_CONFIRMATION IDS_CMD_OPEN_DEVDEPOT IDS_CMD_CLOSE_DEVDEPOT IDS_DEVDEPOT_VERSION_PRESENT_PREFIX IDS_ERR_DEPOT_CORRUPTED IDS_ERR_DEPOT_CONFIG_VERSION IDS_DISTRIBUTIVEFILE IDS_DISTRIBUTIVEFILEONLYSTR IDS_SHOWDISTRIBUTIVEFILE IDS_FROMDISTRIBUTIVEFILE IDS_ORDERDISTRIBUTIVEFILE IDS_MERGERULE_DISTRIBUTIVEFILE IDS_MERGERULE_MERGEDISTRIBUTIVEFILE IDS_PRESENTDISTRIBUTIVEFILE IDS_CHECKSUBSYSTEMDISTRIBUTIVEFILE IDS_CMD_OPEN_DEV_DEPOT_VERSION IDS_DEVDEPOT_GOTO_DEPOT_HISTORY_TEXT IDS_MODULERULE IDS_INCLUDETEXT IDS_NOTINCLUDETEXT IDS_CMD_EDIT IDS_CMD_OPEN_DEV_OBJECT_VERSION IDS_DEVDEPOT_GOTO_OBJECT_HISTORY_TEXT IDS_DEVDEPOT_GOTO_DEPOT_HISTORY_COMMENT IDS_DEVDEPOT_GOTO_OBJECT_HISTORY_COMMENT IDS_CMD_COMPARE_DEV_OBJECTS IDS_CMD_COMPARE_DEV_OBJECTS_VERSION_TEXT IDS_CMD_COMPARE_DEV_OBJECTS_VERSION_COMMENT IDS_CMD_COMPARE_DEVDEPOT_SNAPSHOT_TEXT IDS_CMD_COMPARE_DEVDEPOT_SNAPSHOT_COMMENT IDS_CMD_SHOW_REMOVED_DEV_OBJECTS IDS_QUEST_SHOW_FULL_CONFIG_MERGE IDS_DEVDEPOT_COMPARE_OBJECT_TEXT IDS_DEVDEPOT_COMPARE_OBJECT_COMMENT IDS_CMD_DEVDEPOT_FILTER_TEXT IDS_CMD_DEVDEPOT_FILTER_COMMENT IDS_QUEST_CANCEL_REVISING_ALL_OBJECTS IDS_DEVOBJECT_REMOVING IDS_DEVOBJECT_ADDING IDS_DEVOBJECT_CHANGING IDS_DEVOBJECT_NOT_CHANGING IDS_DEVOBJECT_ADDED IDS_DEVOBJECT_CHANGED IDS_DEVOBJECT_REMOVED IDS_WARN_BINDING_STRING_MISMATCH IDS_QUEST_CLEAN_REMOVED_REFERENCES IDS_INFO_CLEAN_REMOVED_REFS_2 IDS_CMD_DEV_DEPOT_HISTORY_REPORT IDS_DEVDEPOT_REPORT_DATE IDS_DEVDEPOT_REPORT_TIME IDS_DEVDEPOT_REPORT_BY_OBJECTS IDS_DEVDEPOT_REPORT_BY_VERSIONS IDS_DEVDEPOT_REPORT_BY_COMMENTS IDS_DEVDEPOT_REPORT_VERSION_TEXT IDS_DEVDEPOT_REPORT_USER_TEXT IDS_DEVDEPOT_REPORT_CREATE_DATE_TEXT IDS_DEVDEPOT_REPORT_CREATE_TIME_TEXT IDS_DEVDEPOT_REPORT_CREATE_DATE_TEXT_SHORT IDS_DEVDEPOT_REPORT_CREATE_TIME_TEXT_SHORT IDS_DEVDEPOT_REPORT_ADDED_ACTION_TEXT IDS_DEVDEPOT_REPORT_CHANGED_ACTION_TEXT IDS_DEVDEPOT_REPORT_REMOVED_ACTION_TEXT IDS_DEVDEPOT_REPORT_ADDED_ACTIONS_TEXT IDS_DEVDEPOT_REPORT_CHANGED_ACTIONS_TEXT IDS_DEVDEPOT_REPORT_REMOVED_ACTIONS_TEXT IDS_STATUS_DEPOT_DEVOBJECTS_GET IDS_STATUS_DEPOT_DEVOBJECTS_REVISE IDS_STATUS_DEPOT_DEVOBJECTS_ENROLL_REQUEST IDS_STATUS_DEPOT_DEVOBJECTS_ENROLL IDS_STATUS_DEPOT_DEVOBJECTS_CANCEL_REVISE IDS_VERSION_STRING IDS_UNPROCESSED_OBJECT_FIELD IDS_ERR_CREATEFILES IDS_ERR_DEPOT_OBJECT_ENROLL_REVISE_FAULT IDS_FIND_HELPS IDS_FIND_NOENTRIES IDS_FIND_ASKREPLACEALL IDS_SEARCHFILESPREPARE IDS_SEARCHSIMPLE IDS_SEARCHOPENDOCUMENTS IDS_SEARCHFILES IDS_FIND_STARTSTRING IDS_FIND_ENDTOTAL IDS_CMD_EDITINTERFACEUPDATE IDS_CMD_EDITINTERFACEL0 IDS_CMD_EDITINTERFACEL1 IDS_CMD_EDITINTERFACEL2 IDS_CMD_EDITINTERFACETOMOXEL IDS_CMD_EDITINTERFACECLEARLANG IDS_CMD_EDITINTERFACECOPYLANG IDS_CMD_EDITINTERFACETREE IDS_CMD_EDITINTERFACEACTION IDS_CMD_EDITINTERFACESETTINGS IDS_CMD_EDITINTERFACETEXT IDS_OBJECTSTR IDS_EXPORT_STATUS IDS_CLEARLANGQ IDS_CLEAR_STATUS IDS_COPYLANGQ IDS_COPY_STATUS IDS_NONEGROUPING IDS_SORTLANGUAGEGROUPING IDS_FULLEQUALGROUPING IDS_LANGUAGESANALYZE IDS_SORTING IDS_GROUPING IDP_DIFFDOCKIND_OPEN IDS_REPLACE_STATUS IDS_ITEDITMOXELNAME IDS_NO_ACCESS_RIGHT IDS_WRONGLANG IDS_OBJECTSPREFIX IDS_DIFFERENTVALUES IDS_SEARCHDOCDLGHELP IDS_FIND_PROPERTIES IDS_FIND_MODULES IDS_FIND_ROLES IDS_FIND_FORMELEMENTS IDS_FIND_MOXELS IDS_FIND_INTERFACES IDS_FIND_FLOWCHARTS IDS_CMD_FINDINTEXTS IDS_CMD_REPLACEINTEXTS IDS_CMD_SELECTRESULT IDS_CMD_RELOADSTRINGS IDS_CMD_EDITINTERFACEFROMMOXEL IDS_WRONGLANGS IDS_WRONGDICTMOXEL IDS_ITEDITFROMMOXELNAME IDS_WRONGFORMATDICTMOXEL IDS_WRONGCONTENTDICTMOXEL IDS_LOAD_STATUS IDS_ERR_DEPOT_CLIENT_VERSION_MISMATCH IDS_ERR_DEPOT_CLIENT_VERSION_MISMATCH_DETAILS_1 IDS_ERR_DEPOT_CLIENT_VERSION_MISMATCH_DETAILS_2 IDS_ERR_DEPOT_REMOTEINFONOTAVAIL IDS_ERR_DEPOT_NETWORKERROR IDS_ERR_DEPOT_XMLERROR IDS_CONVERT_CREATE_COPY_DB IDS_CONVERT_COPY_DATA IDS_CONVERT_COPY_USERS IDS_CONVERT_COPY_OBJECTS IDS_CONVERT_COPY_VERSIONS IDS_CONVERT_COPY_HISTORY IDS_CONVERT_COPY_EXTERNALS IDS_CONVERT_COPY_REFS IDS_ERR_DEPOT_COMPATIBILITYMISMATCH IDS_DEVDEPOT_OPTIMIZE_DATA IDS_FSTORAGE_INIT_ERR IDS_FSTORAGE_ROLLBACK_ERR IDS_FSTORAGE_ADD_ERR IDS_FSTORAGE_REMOVE_ERR IDS_FSTORAGE_READFILE_ERR IDS_FSTORAGE_READKEY_ERR IDS_FSTORAGE_WRITEKEY_ERR IDS_FSTORAGE_LOCKED IDS_FSTORAGE_PACK_ERROR IDS_FSTORAGE_UNPACK_ERR IDS_FSTORAGE_TRANSACTION_DIR_MISSED_ERR IDS_DEVDEPOT_CREATE_SNP_TASK_STATUS IDS_DEVDEPOT_CREATE_SNP_INFO_STATUS IDS_DEVDEPOT_CREATE_SNP_GETOBJS_STATUS IDS_DEVDEPOT_CREATE_SNP_CREATE_STATUS IDS_DEVDEPOT_CREATE_SNP_SAVE_STATUS IDS_CONVERT_COLLECT_INFO_STR IDS_FSTORAGE_ACTUALIZE_REFS_STR IDS_DEVDEPOT_VERSION_NOT_FOUND_ERR IDS_DEVDEPOT_VERSION_NUMBER_STR IDS_DEVDEPOT_VERSION_LAST_NUMBER_STR IDS_DEVDEPOT_VERSION_DATE_STR IDS_ERR_DEPOT_UNABLE_OPEN_VERSION_FILE_DESCR IDS_ERR_DEPOT_VERSION_FILE_MISMATCH_DESCR �  !  Rh�  '`��  :ON  �~�N  kXEQ  ���Q  ĉR  MOn  eQ�S  �d�~  !j�W  �NHe  \O(u  �e�N�v   gT�v  �ed"}  ~�V  �{tXT  �O�^�  Hr,g:   (u7b:   �S��Oo`  (u7b�c�S  d"}L�    ͑�eňL�  M�n�e�N  �NHepenc  ���R
N�N  ��9e�S�N  �� Rd��N  *jT�cR  �[a���9e�S  ���c�v�[L�  <h_�vCQ }  �v�Spe  �^�P�v�S�S  M�n��sQ�  X[�P�v!j_  < NT<P>   R�~. . .   M�n�v�O�^  R\OM�n�^  :N�N�eg    R{|. . .   9e�S�N^\�v  �^�P�vM�n  �[a��� Rd�  �e�N�vT�y  �Oo`�v6eƖ  �[a����R
N  �O�^�v�e�N  �e�N�^��\�  YpS�N�v�OX[  YpS�N�vR�^  �T�Hr,g:     �bh��v�e��:   R\O�v�eg:   R\O�v�e��:   �^�P�vM�n:   (WM�n-Nd"}  �� Rd��v�[a�  M�n�^��4xOW  �bh��v�eg:   N��ۏL��S�f  �[a�\���R
N  AQ��ۏL��S�f  �[a�\�� Rd�  �N�O�^�e�N-N  (W�e�N-Nd"}  �[a�\��9e�S  (W�O�^�e�N-N  �v�[x.   �v&{T. m x l   �e�lgbL��d\O:   R�g�Q�[. . .   �[a�N����9e�S  	��bM�n�v�e�N  ���Cg)RN��!   �[a��v�^�P�S�S  �[a�_(u�v�S�S  �N�^�P�vM�n-N  �[a�'`���v�S�S  T�O�^!k�^T�T  � ��vT�vR{|  ĉ�[Rh��v	��b  N�cP�ۏL��S�f  M�nN/f�O�^.   �k��M�n�vHr,g  �^�PM�n�v!k�^  �O�^�e�N�v!k�^  �[a�!jWW�v�O�^  	��bd"}�v�~�g   NR� ��vT�v  N��\�M�n�^.   :NM�n�^R\O�[a�  :NM�n�^dƖ�Oo`  :NM�n�^��U_�Oo`  	c�^�P�S�S�v�bh�  �e�N�]�~��	�}Y.   �^penc�v�OYuOS  �N�NMR�vHr,g�m�R  �vM�neg�f�e  �e�N	g�vT�y  c�[�vL�*g~b0R.   �NM�n�^�_0R�[a�  R\OM�n�^�v�~�g  (WSb _�ech-Nd"}  (u7b�l	g��nx��.    Rd� NR�eW[T?   �N�^�P�vM�n	�}Y  �b�[a�>e(WM�n�^  �NHe NR�eW[T?   �[a�(WM�n�^NX[(W  �[a���vQ�N(u7b�b�S  �[a��l	g��(u7b�b�S  kXEQ�v� �*g	�}Y.   	gT��|�vM�nNX[(W  (WM�n�^-N�b�S�[a�  �k��2 *N�[a��vHr,g  �v&{T�v�e�N*gĉ�[  �N1 �O�^�e�N-N	�}Y  	��b�f�eM�n�v�e�N  	��b�O�^M�n�v�e�N  N��Sb _Hr,g�v�e�N  �c�S�v�eW[. m x l   sQ�N�����[a��v�bh�  ~b0ReQ�S�vpeϑ:     sQ�N�^�PHr,g�v�bh�   Rd�M�n�^�v(u7bT?   M�nNvQ�[�^�P	gsQ.   penc�^NM�n�^�esQ.   �bbcL�>f:y:Nhb_�~�g  ㉋S�e�N�^�e�Su��  R\O�^�Ppenc�^�v�~�g  	��b0RM�n�^�v  ��_  T�^�PM�n�v!k�^�~T  SbS�e�N�^�e�Su��  �k���[a��vHr,g. . .   6r�[a����b�S(WM�n�^  M�nHr,gYpS�N�v�~�^  >f:y�TM�n�v�k��T?   egꁨPX[0W�[a��v6e0R  �d ��e�N�^�e�Su��  	c�O�^�e�N�vP[�|�~h�Q  �N�^�PR�e
 �N�^�PR�e  �[a��S(W�O�^�e�N-NX[(W  b__
 >f:yM�n�^�vb__  �e�NibU\T�^�N��S�� _  N��S�b�[a�!jWW�v�eW[  �[a��S(W�^�P�vM�nX[(W  ��n. . . 
 ��n
 ��n  sQ�NM�n�e�Ne�EQ�v�Oo`  �b�s	g�v>f:y(W�O�^�e�N  �[a��l	g��9e�S:   % s   �[a���>e(W�^�P:   % s   M�n�^-N�v�[a�*g~b0R.   �b�[a�>e(WM�n�^. . .   �bX[(W�v�S>f:y(W�^�P-N  (u7b�]�~��nx��(W�^�P.   R�YS�e�N�^�e�Su��   ����OYuM�negR\O�e�N  �NM�n�^���[a�. . .   �_0RM�n�^�v�[a�. . .   �b�SM�n�^�v�[a�. . .   �TM�n�v�[a��k��. . .   �[a��v�b�S���S�m:   % s   �~THr,g
 �~T�^�P�vHr,g   ����_0R�[a�eggbL��d\O:   �QY(W�e�N-N�vd"}. . .   �S�m(WM�n�^-N�v�[a��b�S  R\O�O�^�e�N�e�Su��.   ���_ NR��9e�S�v�[a�T?   6r�[a��l	g���b�S(WM�n�^  sQ�N�^�PHr,g�lʑ  �v�bh�  �l	g	��bM�neggbL��f�e.   (W,gM�nN�e�lgbL��f�e.   	c�^�PM�n�vP[�|�~egh�Q   ����f�eM�n�^egR\O�e�N  R\O�O�^�T�f�eM�n�v�e�N  �S�sc�[0R Rd����N�v_(u  R\O�O�^�vM�n�e�N. . .   h�QR{|�v� �egYT�cR  �l	g��Y�vCg)ReggbL��d\O.   �[a��v�S�S
 Sb _�[a��v�S�S  �S�mM�n�^�[a��v�b�S. . .   gbL�M�n�^�d\O�e�Su��.   sQ핓^�P. . . 
 sQ�M�n�^  ���S�e�N�^�v�e�N�e�Su��  Sb _�^�P. . . 
 Sb _M�n�^  �N�e�N�^ Rd��e�N�e�Su��  �v&{T�v�e�N	g�v<h_.   �k��2 *Nh�Q�vM�n�^�vHr,g  M�n�^�]\O/f�SuR{|��.   �n� ��T�vh� ��^	g�]+R.   �[a�/f�N�^�P�c�S�v:   % s   �^�P
 Sb _M�n�^�]\O�v�z�S  �[a�/f:N���b�S�v:   % s   �[a�/f�NM�n Rd��v:   % s   �S�mM�n�^�v�[a��e�Su��:   �S�sc�[0R�^�P-N Rd��v�[a�:   M�n�^�vg�RhV�vHr,g:   % s   6r�[a���vQ�N(u7b�b�S(WM�n�^  �k��h�Q�v�[a��T�����vM�n  �b�[a�>e(WM�n�^�e�Su��.   �NM�n�^�_0R�[a��e�Su��.   �b�SM�n�^�v�[a��e�Su��.   �^�P�v�S�S
 Sb _M�n�^�v�S�S  �e�lR�egbL��^�P�]\O�v(u7b.   (WX[�P0WNX[(WM�n�v�T�Hr,g  nx���[x��! 
 �Q.�eQ�[x.   �[a��l	g���b�Seg��:   % s   �b�[a�>e(WM�n�^�v��Bl. . .   �b�e�N�m�R0R�e�N�^�e�Su��  M�n�vHr,g�T�vh�vJS,g�vT.   
 % s 
 0W@W�vM�n�^*g~b0R.   X[�S�^�P�vpenc�^�e�Su��.   N��(Wg�RhV
NO(u�v�Oo`.   �^�P�vHr,g�Tz�^�vHr,gN|Q�[.    Rd�M�n�^�]\O�e�SuQ�~��.   �[7b�z�^(uz�^�vHr,g:     % s   �bsQ.��e�N�Q0R�e�N�^�e�Su��  nd��nhV
 nd�>f:yM�n�^�v�nhV  >f:y Rd��v
  Rd��[a��v>f:yh��  Sb _:N,g�[a�	�}Y�vM�n�^�v�S�S  ���S�e�N�^�vsQ.��e�N�e�Su��  	�}Y�v�vU_NX[(W. 
 R\O�vU_T?    Rd�M�n�^�]\O�e�SuX M L ��.   9e�S' % s ' (u7b�v�[x�e�Su��  �S�s�b�S�v�[a�. 
 �e�lgbL��d\O.   �[a���vQ�N(u7b�b�Seg��:   % s   qQX[�SM�n�^�e�Su��: 
 
 % s   �c
N0RM�n�^�vpenc�^MOn��9e�S.   ��' % s ' (u7b�v�Spe�e�Su��.   �e�l Rd�wQ	g�{tCg)R�v/U N�v(u7b.   R\O�e�N�v�d\O��BlHr,gkXEQ�v'`��.   kXEQ�eW[. . . 
 kXEQ�eW[
 kXEQ�eW[  �NHe�eW[. . . 
 �NHe�eW[
 �NHe�eW[  �c
N0R
 % s 0W@W�vM�n�^�e�Su��   ����Sbc�^�PegO(u,gz�^�vHrb e .   wQ	g,gT�y�v�e�N�]�~X[(W. �N�fT?   (W
 % s 0W@W
NR\OM�n�^�e�Su��   Rd��eW[. . . 
  Rd��eW[
  Rd��eW[  �s	gޏ�c�v(u7bN�sL��v(u7bN N7h.   - - - -   M�n�^�v�d\O�[b  - - - -   R\O�O�^�T�f�eM�n�v�e�N�vǏz�[b.   Sb _M�n�vHr,g
 Sb _,g�^�PHr,g�vM�n  )�w0RHr,g
  Rd� gTHr,g( S�b,gHr,g  �Sl�0RHr,g
  Rd� gTHr,g( S�b,gHr,g    T
 % s 0W@W�vM�n�^T��|�e�Su��  R\O�e�N�v�d\O��Bl�O�^�kXEQ�v'`��.   - - - -    _�YM�n�^�v�d\O  - - - -   �S�m0R�^�P>e0R�[a��v�b�S�e�Su��.   �e�N�]�~/f\O:NvQ�[{|�W�v�echSb _�v.   - - - - M�n�^�v�d\O���S�m  - - - -   	��b. . . 
 	��b�s�[M�n�^�vag�N
 �nhV  wQ	g' % s ' T�y�vM�n�^�v(u7b*g~b0R.   �b' % s ' (u7b Rd��NM�n�^�e�Su��.   �b' % s ' (u7b�m�R0RM�n�^�e�Su��.   �v&{T�e�N� ��vbRN&{Tĉ�[�v� �.   wQ	g' % s ' T�yM�n�^�v(u7b�]�~X[(W.   R\O�e�N�v�d\O��Bl�OYuM�n. 
 gbL�T?   R\O�e�N�v�d\O��Bl�f�eM�n. 
 gbL�T?   ���N�^�PM�n�N�f�sL��vM�n. 
 �~�~T?   �b' % s ' (u7bb`Y(WM�n�^�e�Su��.   �^�P-NX[(WwQ	g  ' % s ' T�y Rd��v(u7b  �f�e�^�P�vM�n
 �_0R�^�P-NM�n g�e�vHr,g  c�[�v��_�]�~	g�^�P, �e�lR\O�e�v�^�P.   �c
N0R�^�P. . . 
 �c
N0R�^�P
 �c
N0R�^�P  �O�^�v��n. . . 
 �O�^�v��n
 �O�^�v��n  M�n�vT�yN�O�^��vM�n N7h. 
 �~�~T?   �r�Q
 ͑�e�[wsQ�N(WM�n�^�OYu�[a��v�Oo`  N��nZi�v_(u. 
 ���`(uKb Rd�_(u.   gbL��d\O�e�sL�M�nEQR���N�f. 
 �~�~T?   '   % s ' �[a�S�bc�[0R Rd��[a��v_(u:   Sb _�[a��vHr,g
 Sb _,g�^�PHr,g�vc�[�[a�  hQzd"}. . . 
 �eW[-N�vhQzd"}
 hQzd"}  hQz�f�N. . . 
 �eW[-N�vhQz�f�N
 hQz�f�N  R\O�^�P. . . 
 R\OM�n�v�e�^�P
 R\O�^�P  ;Na! ! !   penc�^�vMOn9e�S�N. 
 �~�~T?   R\O���S. . . 
 :N�^�PM�n gT�vHr,gR\O���S  N�S�NnZi/U N�v	gُ7hCg�v(u7b�S�S�e�NCg
 .   �e�leR:Y
 wQ	g�{tCg)R�v/U N(u7b�v�{tCg)R.   M�n�^��Bl�SbcegO(uz�^�v�eHr,g. 
 �~�~T?   ,g(u7b�]�~�b	g, 
 T,gM�n�^T��|�vM�n. 
 �~�~T?   >e(W�^�P. . . 
 �b�[a��v�eHr,g�e(WM�n�^
 >e(W�^�P  �[7b�z�^(uz�^�TM�n�^�vg�RhV	g�N�vN|Q�[�vHr,g.   _N��(WM�.U�[a���OS�vX[�P0��\Ջ�y�RirSO�vX[�P�^  X[�PhV�vO(uN�S�����1 C �ONHr,g�THr,g% s T�e   Rd��v_(u�e�Su��. 
 �S�s�S���e_X[�S�v�[a�.   �N�^�PňeQM�n. . . 
 �N�^�P	�}Y�vHr,g�N�f�sL��vM�n  ���Q0Rh�<h�ech. . . 
 ���Q0Rh�<h�ech
 ���Q0Rh�<h�ech  ���c�S�v�eW[. . . 
 ���c�S�v�eW[
 ���c�S�v�eW[  �T�^�P�[a��v�k. . . 
 �k���[a��T�^�P-N�[a� gT�vHr,g  (WM��^���N�����e�Su��! 
 �h�g(u7b�v�l�QT�y�T�[x.   �b�^�P�T�e�N�vM�n�v�k. . . 
 �k�^�P�vM�n�T�e�N�vM�n  �N�^�P���_. . . 
 �NM�n�^���_�[a��v�eHr,g
 �N�^�P���_  
 wQ	gvQ�N
 T,gM�n�^T��|�vpenc�^�v(u7bgbL�T��|�v\Ջ.     ' % s ' �[a�  ��9e�S
 v^*geg���_0R�NM�n�^. 
 �~�~T?    Rd�  ' % s ' (u7b�e�Su��
 .   (u7b/fTM�n�^T��|�v.   �b�[a�>e(W�^�P�e�Su��. 
  ����b�[a�>e(W�^�PeggbL��d\O:   (W�^�P-N�b�S. . . 
 h�Q:N(W9e�SǏz-N�v�[a�
 (W�^�P-N�b�S  MOnb��Oo`�^ޏ�c�v{|�WN&{T(WM�n�^{v���v, �~�~�c
NT?   �S�m�[a��v�b�S�NT
 (u7bN���bgbL��v�S�f>e7��^�P. 
 �~�~T  gbL��Sl��v�d\O�esQ�N���Sl��[a��vHr,g
 \�� Rd�. 
 �~�~T?   (W�^�P-N�b�S�[a��e�Su��. 
 �S�s	g�N�[a���vQ�N(u7b�b�S.   �bM�n�k��/ �~TN�^�P
 �bM�n�k���T/ b��~TN�����vM�n  ĉ�[���b�S�[a��vh���e
 ���b�S�[a��v�S�fO1Y�S. 
 ĉ�[h��T?   �^�P�v�{t. . . 
 Sb _O݋:N�v/fgbL�M�n�^�v�{t�d\O. 
 �^�P�v�{t  �S�mM�n�^�v�[a��e�Su��. 
 	g�S���b�S�[a��v(u7bck(WgbL��^�P�v�]\O.   �Sbc�^�PHr,g�vM�n�e�Su��. 
 s^�S�sL��vHr,g�e�lO(u,g�^�PHr,g�vM�n.   �b�^�P�vM�n�OYu0R�e�N. . . 
 �bM�n�^�OYu0R�e�N
 �b�^�P�vM�n�OYu0R�e�N  �b�[a�>e(W�^�P�e�Su��. 
 �S�swQ	gc�[0R Rd��[a�_(u�v�[a�. 
  Rd��v_(u?   �fbc NReQ�S�e��Sb _�_Y�ech�
 ����Bl�fY�|�~D��n0
 �`���~�~�fbceQ�S�vǏzT�  �S�m(W�^�P�v�b�S. . . 
 �S�m�[a��v�S�fv^�NM�n�^�_0R�[a��vHr,g
 �S�m(W�^�P�v�b�S  )�R\O�O�^v^�f�eM�n�v�e�N. . . 
 R\O�O�^v^�f�eM�n�v�e�N
 R\O�O�^v^�f�eM�n�v�e�N  +�gbL��d\O�eHr,g�v�Oo`\��T�T. 
 TbHr,g�v�S\&{T gT�v��T�THr,g�v�S. 
 �~�~T?   4�wQ	g' % s '   T�y�v(u7b�e1\�� Rd��N�^�P. 
 b`Y Rd��v(u7bv^�Nc�[Cg)R�T�[x�f�N(u7b�NMR�vCg)R�T�[xT?   M�M�n�^�v*`_!j_��9e�S. 
 ��Bl�Qޏ�ceggbL��^�v�]\O. 
 
 Sb _M�n�eb�
 �S�mN�^�vޏ�c�NTgbL��^�v�d\O�egbL�N�^�vޏ�c. 
 ( M�n܃US�vy�/ M�n�^/ sQ핓^)   s�52>7<>6=>  4>1028BL  ?>;L7>20B5;O  E@0=8;8I0  :>=D83C@0F88  A  8<5=5<  ' % s '   87- 70  A>2?045=8O  845=B8D8:0B>@0  ?>;L7>20B5;O.   e, ����,�o�����>�%�����0uL�g �����y������  5 ����!Ph�(F��4G!�0���d�![!��r!�����k'
��%-#�#�#Z#�#C#s##�����
zN+ �f
�
h1Nh��
-�<C�Q%���
�
��U4�%t%P%�%,%�5�q�#�mn/N
���B�3���E�&e&"&E&�*z^:�Nd��G� �,��G@��	�BCi�	8�9"_"�"�#��|!G
�,^�[��"R��d_[=��"Jj��&�[	9	��	�"
�&�"�Nn^��"3!� �!� !�� � a&F$$�$�$s$�$[$,$%�$�$� il��!T �)"P�g�Z��yh 
� `+z=� � � ��� �� � �AY������ 	~A ��&	�� �����H ������\|��{	*��>	O z�zl��"�!�!| �!j
� wFFClF��� � n�{<Z�����j���	��\4
B�
S� �  �	A	/	w ��s���2 >3�X��	)� �Y T�R+l��6^ z�� c �7�i���$ �	���: D
��}
[A��	����8�������/�J� � 	� � �b� b��eh h �\pm m DAr r F B � . z	 % �� O| r�� ]� � � >y��h�
��O�	
����k��&"2�:�	�	����>����/W
�Me	S	.��
D<�H���
X�
�
�Z��� \* � Rf6 � J O  ! � ' T d� ��� �Jrf@�2'� ]�t�R� ��*��3�	 [�7  �V�H7> �1
	��w	u0) y�����ggA�� OkN�,,�� ��� ���� ���'6�4������/�
 t����