  �'      ResB             �.     C  �/  �/  �       "  IDS_SETPROP_SESSIONPARAMSETTINGS_ONLY IDS_EDITBTNTOOLTIP_CLEAR IDS_EDITBTNTOOLTIP_LISTSEL IDS_EDITBTNTOOLTIP_SPECSEL IDS_EDITBTNTOOLTIP_OPEN IDS_EDITBTNTOOLTIP_UP IDS_EDITBTNTOOLTIP_DOWN IDS_EDITBTNTOOLTIP_DROPLIST IDS_ADDIN_TITLE IDS_FSE_TITLE IDS_CPE_TITLE IDS_AGENT_TITLE IDS_CRYPTOEXTENSION IDS_HTML_COPYRIGHT IDS_HTML_RUN_MODE_DESCR IDS_HTML_EXTERN_MODULE_INSTALL_IN_PROGRESS IDS_HTML_EXTERN_MODULE_INSTALLED IDS_HTML_EXTERN_MODULE_ALREADY_INSTALLED IDS_HTML_EXTERN_MODULE_INSTALL_FAILED IDS_CONFIRM_INSTALL_IE IDS_VERIFY_PUBLISHER IDS_INSTALL_NOW_IE IDS_PRESS_CONTINUE IDS_FILE_EXT_INSTALL IDS_CRYPTO_EXT_INSTALL IDS_AGENT_EXT_INSTALL IDS_EXT_MODULE_INSTALL IDS_CHROME_EXT_INSTALL_AGREE_MSG IDS_CHROME_EXT_INSTALL_REJECT_MSG IDS_SAFARI_EXT_INSTALL_TIP_MACOS IDS_SAFARI_EXT_INSTALL_DOWNLOAD IDS_CHROME_37_EXT_INSTALL_BEGIN IDS_CHROME_37_EXT_INSTALL_FILES_EXT IDS_CHROME_37_EXT_INSTALL_CRYPTO_EXT IDS_CHROME_37_EXT_INSTALL_AGENT_EXT IDS_CHROME_37_EXT_INSTALL_ADDIN_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT IDS_CHROME_37_EXT_INSTALL_WEB_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LIN IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT IDS_CHROME_37_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_CHROME_37_EXT_INSTALL_NEED_RESTART IDS_CHROME_37_EXT_INSTALL_WAIT IDS_CHROME_37_EXT_INSTALL_WAIT_LIN IDS_CHROME_37_EXT_INSTALL_RESTART IDS_EDGE_START_IN_IE11 IDS_CHROME_CLIPBOARD_EXT_NOT_INSTALLED IDS_CHROME_CLIPBOARD_EXT_NEED_RESTART IDS_CHROME_EXT_ATTEMPT_TO_USE IDS_CHROME_EXT_INSTALL_MAYBE_LATER IDS_FF_EXT_INSTALL_WEB_EXT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT IDS_FF_EXT_INSTALL_HOST_EXT_LEFT_LIN IDS_FF_CLIPBOARD_EXT_NOT_INSTALLED IDS_HTML_BROWSER_SETTINGS_INFO_CONFIRMATION IDS_HTML_COLOR_CHOOSE IDS_HTML_RED IDS_HTML_GREEN IDS_HTML_BLUE IDS_HTML_BACKGOUND_COLOR_PREVIEW IDS_HTML_TEXT_COLOR_PREVIEW IDS_HTML_OPEN_CALCULATOR IDS_HTML_OPEN_CALENDAR IDS_HTML_COPY_TO_CLIPBOARD_AS_NUMBER IDS_HTML_ADD_NUMBER_TO_CLIPBOARD IDS_HTML_SUBTRACT_NUMBER_FROM_CLIPBOARD IDS_HTML_SERVICE IDS_HTML_FORM_CUSTOMIZATION IDS_HTML_TOOLBAR_CUSTOMIZE IDS_HTML_ADD_REMOVE_BUTTONS IDS_HTML_ADD_GROUP IDS_HTML_ADD_FIELDS IDS_HTML_DELETE_CURRENT_ITEM IDS_HTML_MOVE_CURRENT_ITEM_UP IDS_HTML_MOVE_CURRENT_ITEM_DOWN IDS_HTML_MARK_ALL IDS_HTML_UNMARK_ALL IDS_HTML_DELETE IDS_HTML_MOVE_UP IDS_HTML_MOVE_DOWN IDS_HTML_RESTORE_DEFAULT_SETTINGS IDS_HTML_FORM_ITEM_PROPERTIES IDS_HTML_CHOOSE_FIELDS_TO_PLACE_ON_FORM IDS_HTML_LOADING IDS_HTML_THEMES_PANEL IDS_HTML_NAVIGATION_PANEL IDS_HTML_NAVIGATION2_PANEL IDS_HTML_OPEN_HELP IDS_HTML_SHOW_ABOUT IDS_HTML_FAVORITE_LINKS IDS_HTML_FAVORITES IDS_HTML_HISTORY IDS_HTML_ENTER_NAME_AND_PASSWORD IDS_HTML_EXIT IDS_HTML_REPEAT_ENTER IDS_HTML_FILE_UPLOADING IDS_HTML_FILE IDS_HTML_UPLOADING IDS_HTML_APPLY IDS_HTML_SHOW_BUTTON IDS_HTML_HISTORY_TITLE IDS_HTML_TOPICSELECTOR_TITLE IDS_HTML_FILE_DOWNLOADING IDS_RTE_ACTIVEDOCUMENTGET_ERROR IDS_RTE_CONNECTION_UNEXPECTEDLY_CLOSED IDS_RESET_TOOLBAR IDS_HTML_CLIPSTORE IDS_HTML_CLIPPLUS IDS_HTML_CLIPMINUS IDS_HTML_WINDOW_ACTIVATED_MSG IDS_HTML_UNABLE_TO_SWITCH_TO_MAINWINDOW IDS_HTML_CLIPBOARD_INACCESSIBLE IDS_HTML_USE_BROWSER_CLIPBOARD_COMMANDS IDS_HTML_CLIPBOARD_OPERATION_DISABLED IDS_TE_TITLE IDS_TE_MESSAGE IDS_TE_INTERRUPT IDS_CMD_HELPTRAINING IDS_WEB_COMPAT_PLATFORM_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_UNSUPPORTED IDS_WEB_COMPAT_BROWSER_VERSION_UNSUPPORTED IDS_WEB_COMPAT_WORK_IMPOSSIBLE IDS_WEB_BROWSERSLIST_HEADER IDS_WEB_BROWSERSLIST_DOWNLOAD IDS_WEB_BROWSERSLIST_VERSION_IE IDS_WEB_BROWSERSLIST_VERSION_FF IDS_WEB_BROWSERSLIST_VERSION_CHROME IDS_WEB_BROWSERSLIST_VERSION_SAFARI IDS_WEB_BROWSERSLIST_VERSION_SAFARI_IPAD IDS_WEB_BROWSER_UPDATE_LINK_IE IDS_WEB_BROWSER_UPDATE_LINK_SAFARI IDS_WEB_BROWSER_UPDATE_LINK_SAFARI_IPAD IDS_HTML_SET_WARN_EXT_COMP IDS_HTML_SET_GET_FILE IDS_HTML_SET_WARN_LINK_GET_FILE IDS_EXECUTE_NOT_SUPPORTED IDS_SEARCH_PROCESSING IDS_LOGOUT_WORK_FINISH IDS_WORK_IN_FULLSCREEN IDS_WORK_ONLY_IN_FULLSCREEN IDS_START_WORKING IDS_CONTINUE_WORK IDS_ECS_BROWSER_NOT_SUPPORTED IDS_ECS_MEDIA_REQUIRED_SECURE_ORIGIN IDS_ECS_MEDIA_NOT_AVAILABLE IDC_OIDC_STANDARD_LOGIN_NAME IDC_OIDC_ANOTHER_SERVICES IDC_OIDC_ERROR browsersettingsinfoieru.html browsersettingsinfoieen.html browsersettingsinfochru.html browsersettingsinfochen.html browsersettingsinfosfru.html browsersettingsinfosfen.html browsersettingsinfoff.html browsersettingsinfomain.html   !8=  ;57  $09;  !20;8  5;5=  !5@287  8F5=7  >1028  72048  >:068  0<0;8  7B@89  '5@25=  @5H:0  <?>@B  AB>@8O  #25;8G8  71@0=>  @>;>68  @5:JA=8  <?>@B. . .   >  A:>@>!   "J@A5=5. . .   >1028  ?>;5  ;57  >B=>2>  715@8  ( F 4 )   @C38  1CB>=8  @8<5@5=  D>=  >1028  3@C?0  0=5;  @0745;8  #G51=0  15@A8O  71>@  =0  F2OB  0?>G=8  @01>B0  <?>@B  =0  D09;  02J@H8  @01>B0  @8<5@5=  B5:AB  71>@  =0  3;020  0=5;  459AB28O  @5<5AB8  =03>@5  0=5;  =02830F8O  B2>@8  :0;5=40@  0@:8@09  2A8G:8  @5<5AB8  =04>;C  @>4J;68  @01>B0  B2>@8  A?@02:0B0   5AB0@B8@09  A530  2J=He =  :><?>=5=B  !20;O=5  =0  ;5=B0  715@8  >B  A?8AJ:0  '    #  #!#  71@0=8  ?@5?@0B:8  >A5B5=8  AB@0=8F8  B2>@8  :0;:C;0B>@  7G8AB8  ( S h i f t + F 4 )   0AB@>9:0  =0  D>@<0  >?8@09  :0B>  G8A;>  >;CG020=5  =0  D09;  B;>68  8=AB0;0F8OB0  0?>G=8  8=AB0;0F8OB0  " =AB0;8@09  /   I n s t a l l "   B2>@8  ( C t r l + S h i f t + F 4 )   7?J;=O20=5  =0  7040=8O  7B@89  B5:CI8O  5;5<5=B  <?>@B8@09B5  :><?>=5=B0  ?5@0F8OB0  5  =54>ABJ?=0.   0:  40  =0AB@>8<  1@0C7J@0?   845>:0<5@0B0  =5  5  4>ABJ?=0   01>B0  2  ?J;=>5:@0=5=  @568<  7?J;=8  ?>2B>@=>  AB0@B8@0=5?   h t t p : / / w w w . a p p l e . c o m / r u / i o s /   !2>9AB20  =0  5;5<5=B  =0  D>@<0  !?8AJ:  A  ?>44J@60=8  1@0C7J@8  0409  AB0=40@B=8  =0AB@>9:8. . .   @5<5AB8  B5:CI8O  5;5<5=B  3>@5  @5<5AB8  B5:CI8O  5;5<5=B  4>;C  @07H8@5=85  70  @01>B0  A  D09;>25  715@8  >B  A?8AJ:0  ( C t r l   +   D o w n )   >:068  8=D>@<0F8O  70  ?@>3@0<0B0  >102O=5  8;8  87B@820=5  =0  1CB>=8  72048  G8A;>  >B  1CD5@0  =0  >1<5=0  =AB0;8@0=5  =0  2J=H5=  :><?>=5=B.   @81028  G8A;>  :J<  1CD5@0  =0  >1<5=0  !20;8  >B<5B:0B0  >B  2A8G:8  5;5<5=B8   07H8@5=85  70  @01>B0  A  :@8?B>3@0D8O  >?8@09  2  1CD5@0  =0  >1<5=0  :0B>  G8A;>  J=H=8OB  :><?>=5=B  5  8=AB0;8@0=  CA?5H=>.   715@5B5  ?>;5B0  70  ?>AB02O=5  2J2  D>@<0B0  h t t p : / / w w w . a p p l e . c o m / r u / s a f a r i / d o w n l o a d /   +� 01>B0  5  2J7<>6=0  A0<>  2  ?J;=>5:@0=5=  @568<  +ܫ 1 !: @54?@8OB85     8725ABO20=5  8  AB0@B8@0=5�   ,�7?J;=O20  A5  8=AB0;8@0=5  =0  2J=H5=  :><?>=5=B  -�?5@0B>@  7?J;=8  2  C51- :;85=B0  =5  A5  ?>44J@60  .�72J@H20  A5  8=AB0;0F8O  =0  2J=H=8O  :><?>=5=B. . .   .�0  =0G0;>  =0  8=AB0;0F8OB0  8715@5B5  " @>4J;68" .   /�:>  D09;0  =5  5  ?>;CG5=,   87?>;7209B5  =0AB@>9:0B0  0�=AB0;8@0=5  =0  @07H8@5=85  70  @01>B0B0  A  D09;>25.   0�>A>G20=5  =0  @07H8@5=5  70  @01>B0  A  :@8?B>70I8B0.   1ܩ   " 1 !- !>DB"   ,   1 9 9 6 - 2 0 1 8 .   A8G:8  ?@020  70?075=8  2�7?>;720=8O  1@0C7J@  =5  ?>44J@60  @01>B0  A  1 !: 80;>3  2�J7=8:=0  2@5<5==>  ?@5:JA20=5  =0  8=B5@=5B  2@J7:0B0.   2�J2545B5  ?>B@518B5;A:>  8<5  8  ?0@>;0  1 !: @54?@8OB85  5�5  <>65  40  1J45  87?J;=5=  ?@5E>4  :J<  3;02=8O  ?@>7>@5F.   7�  1@0C7J@JB  8  =5  5  @07@5H5=>  87?>;720=5B>  =0  :;8?1>@4.   9�72J@H20  A5  8=AB0;8@0=5  =0  @07H8@5=85  =0  @01>B0  A  D09;>25  :�C O M - >15:B8  A5  ?>44J@60B  A0<>  2  >?5@0F8>==8  A8AB5<8  W i n d o w s   ?�0  4>ABJ?  4>  2845>:0<5@0B0  5  =5>1E>48<>  70I8B5=0  2@J7:0  ( h t t p s )   ?�7?J;=O20  A5  8=AB0;8@0=5  =0  @07H8@5=85  70  @01>B0  A  :@8?B>3@0D8O  @�AJI5AB2O20  A5  >1@01>B:0  =0  D>=>28  7040=8O.   
 >;O,   87G0:09B5  . . .   A�>44J@60=8  25@A88:   1 0   8  ?>- =>28< b r / > @5?>@JG0=0  25@A8O:   ?>A;54=0.   A�>44J@60=0  25@A8O:   5 2   8  ?>- =>20< b r / > @5?>@JG0=0  25@A8O:   ?>A;54=0.   B�72J@H5=  5  ?@5E>4  :J<  ?@>7>@5F0.   !5;5:B8@09B5    70  40  ?@>4J;68B5.   C�>44J@60=8  25@A88:   4 . 0   8  ?>- =>20< b r / > @5?>@JG20=0  25@A8O:   ?>A;54=0.   D�=AB0;8@0=5  =0  ?@>3@0<0  � 1 !: @54?@8OB85     8725ABO20=5  8  AB0@B8@0=5� .   E�h t t p : / / w i n d o w s . m i c r o s o f t . c o m / r u - R U / i n t e r n e t - e x p l o r e r / p r o d u c t s / i e / h o m e   E�>44J@60=0  25@A8O:   4 . 0 . 5   8  ?>- =>20< b r / > @5?>@JG20=0  25@A8O:   ?>A;54=0.   F�J=H=8OB  :><?>=5=B0  5  8=AB0;8@0=  CA?5H=>  8;8  5  18;  8=AB0;8@0=  ?>- @0=>.   L�J=H=8OB  :><?>=5=B  =5  5  8=AB0;8@0=! 
   ?@>F5A0  =0  8=AB0;0F8O  2J7=8:=0  3@5H:0!   O�7?J;=O20  A5  8=AB0;0F8O  =0  ?@>3@0<0  � 1 !: @54?@8OB85     8725ABO20=5  8  AB0@B8@0=5�   O�>44J@60=8  25@A88:   4 . 0 . 4   ( i O S   3 . 2 )   8  ?>- =>20< b r / > @5?>@JG20=0  25@A8O:   ?>A;54=0.   R�!2>9AB2>B>  =5  <>65  40  1J45  @540:B8@0=>  A;54  8728:20=5  =0  #AB0=>2:00@0<5B@>2!50=A0  S�>65B5  40  87?>;720B5  S t r l + C   70  :>?8@0=5,   C t r l + X   70  87@O720=5  8  C t r l + V   70  ?>AB02O=5.   [�0  40  >B:065B5  8=AB0;0F8OB0,   8715@5B5  " B:068  ?@54020=5B>  /   D i s c a r d "   8  70B2>@5B5  ?@>7>@5F0.   [�@8  ?>O20  =0  70?8B20=5  70  @07@5H020=5  =0  8=AB0;8@0=5  8715@5B5  1CB>=  " =AB0;8@09  /   I n s t a l l " .   _�< p > 0  40  8=AB0;8@0B5  % s ,   =0?@025B5  ?>;CG5=8OB  D09;  87?J;=8<  8  3>  AB0@B8@09B5  70  87?J;=5=85. < / p >   `�< p > 0  40  8=AB0;8@0B5  % s ,   8715@5B5  ?>;CG5=8OB  D09;  8  87G0:09B5  ?@8:;NG20=5B>  =0  8=AB0;0F8OB0. < / p >   ��!;54  CA?5H=>  8=AB0;8@0=5  =0  @07H8@5=85B>  70  1@0C7J@0  5  =5>1E>48<>  40  A5  @5AB0@B8@0  AB@0=8F0B0. 
 715@5B5  "  5AB0@B8@09  A530"   70  @5AB0@B@0=5.   ��!09B  % h o s t n a m e %   ?@028  >?8B  70  4>ABJ?  4>  :;8?1>@40.   715@5B5  ' O K '   70  40  @07@5H8B5  4>ABJ?0  4>  B>78  A09B.   715@5B5  ' B:068'   70  701@0=0  =0  4>ABJ?0.   ��7?>;720=0B0  >B  0A  25@A8O  =0  1@0C7J@0  =5  2;870  2  A?8AJ:0  A  ?>44J@60=8B5  >B  1 !: @54?@8OB85. 
 715@5B5  O K   70  ?@5E>4  :J<  A?8AJ:0  A  ?>44J@60=8  1@0C7J@8.   ��0  40  70?>G=5B5  8=AB0;0F8OB0,   8715@5B5  " 0?@54  /   C o n t i n u e " .   !;54  B>20  ?>B2J@45B5  AJ3;0A85B>  A8  70  8=AB0;8@0=5,   8718@09:8  1CB>=  " =AB0;8@09  /   I n s t a l l " .   ��< p > 5H5  8=AB0;8@0=>  @07H8@5=85  =0  1@0C7J@0.   AB0=0  70  8=AB0;8@0=5  % s . < / p > < p > 0  B078  F5;  =0?@025B5  ?>;CG5=8OB  D09;  87?J;=8<  8  3>  AB0@B8@09B5  70  87?J;=O20=5. < / p > < p >   ��< p > !;54  CA?5H=>  8=AB0;8@0=5  =0  @07H8@5=85B>  70  1@0C7J@0  8  :><?>=5=B8B5  5  =5>1E>48<>  AB@0=8F0B0  40  1J45  @5AB0@B8@0=0.   715@5B5< b > "  5AB0@B8@09  A530"   70  40  @5AB0@B8@0B5. < / p >   ��< p > =AB0;8@0=>  5  @07H8@5=85  70  1@0C7J@0,   AB0=0  40  A5  8=AB0;8@0  % s . < / p > < p > ,   0  B078  F5;  8715@5B5  ?>;CG5=8OB  D09;  ( 2  4>;=0B0  G0AB  =0  5:@0=0)   8  87G0:09B5  ?@8:;NG20=5B>  =0  8=AB0;0F8OB0. < / p > < p >   ��0  4>ABJ?  4>  B078  DC=:F8>=0;=>AB  A5  ?@5?>@JG20  AB0@B8@0=5  =0  ?@8;>65=85B>  2  1@0C7J@  I n t e r n e t   E x p l o r e r . 
 0  B078  F5;  >B2>@5B5  <5=NB0  =0  1@0C7J@0  G@57  1CB>=  " . . . "   8  8715@5B5  ?C=:B  " B2>@8  2  I n t e r n e t   E x p l o r e r " .   ��@8  ?>O20  =0  70?8B20=5  70  8=AB0;0F8O  A5  C25@5B5,   G5  2  :0G5AB2>B>  =0  02B>@  5  ?>A>G5=  4>AB02G8:  =0  :><?>=5=B8,   =0  :>3>B>  8<0B5  4>25@85!   0  >B:07  >B  8=AB0;0F8O  8715@5B5  " B:068  /   C a n c e l " .   0  40  ?>B2J@48B5  8=AB0;0F8OB0  8715@5B5  ��7?>;720=8O  >B  0A  1@0C7J@  =5  2;870  2  A?8AJ:0  A  ?>44J@60=8  >B  1 !: @54?@8OB85. 
  01>B0B0  =0  A8AB5<0B0  <>65  40  1J45  =5:>@5:B=0. 
 715@5B5    70  40  ?@>4J;68B5  @01>B0B0  A8. 
 715@5B5  B:068  ( C a n c e l )   70  ?@5E>4  :J<  A?8AJ:0  A  ?>44J@60=8  1@0C7J@8.   ��7?>;720=0B0  >B  0A  25@A8O  =0  1@0C7J@0  =5  2;870  2  A?8AJ:0  A  ?>44J@60=8  >B  1 !: @54?@8OB85. 
  01>B0B0  =0  A8AB5<0B0  <>65  40  1J45  =5:>@5:B=0. 
 715@5B5    70  40  ?@>4J;68B5  @01>B0B0  A8. 
 715@5B5  B:068  ( C a n c e l )   70  ?@5E>4  :J<  A?8AJ:0  A  ?>44J@60=8  1@0C7J@8.   ��< p > 0  87?J;=O20=5  =0  459AB285B>  5  =5>1E>4<>  40  8=AB0;8@0B5  % s . < / p > < p > 0  =0G0;>  =0  8=AB0;0F8OB0  8715@5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > )5  1J45  87?J;=5=  8<?>@B  =0  :><?>=5=B0.   !;54  B>20  8715@5B5  ?>;CG5=8OB  D09;  8  87G0:09B5  ?@8:;NG20=5  =0  8=AB0;0F8OB0. < / p >    �< p > 0  40  87?J;=8B5  459AB285B>  5  =5>1E>48<>  40  8=AB0;8@0B5  % s . < / p > < p > 0  =0G0;>  =0  8=AB0;0F8OB0  8715@5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > )5  1J45  87?J;=5=  8<?>@B  =0  :><?>=5=B8B5.   !;54  B>20  =0?@025B5  ?>;CG5=8OB  D09;  87?J;=8<  8  3>  AB0@B8@09B5  70  87?J;=O20=5. < / p >   �< p > 5H5  8=AB0;8@0=>  @07H8@5=8O  =0  1@0C7J@0.   AB0=0  70  8=AB0;8@0=5  % s . < / p > < p > 0  =0G0;>  =0  8=AB0;0F8OB0  =0B8A=5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > )5  1J45  87?J;=5=  8<?>@B  =0  :><?>=5=B8B5.   !;54  B>20  =0?@025B5  ?>;CG5=8OB  D09;  87?J;=8<  8  3>  AB0@B8@09B5  70  87?J;=O20=5. < p >   �7?>;720=0B0  >B  0A  >?5@0F8>==0  A8AB5<0  =5  2;870  2  A?8AJ:0  A  ?>44J@60=8  >B  1 !: @54?@8OB85. 
  01>B0B0  =0  A8AB5<0B0  <>65  40  1J45  =5:>@5:B=0. 
 715@5B5    70  40  ?@>4J;68B5  @01>B0B0  A8. 
 715@5B5  B:068  ( C a n c e l )   70  ?@5E>4  :J<  A?8AJ:0  A  ?>44J@60=8  >?5@0F8>==8  A8AB5<8  8  1@0C7J@8.   �< p >  07H8@5=85B>  =0  1@0C7J@0  15H5  8=AB0;8@0=>.   >?J;=8B5;=>  70  8=AB0;0F8O  % s . < / p > < p > 0  =0G0;>  =0  8=AB0;0F8OB0  8715@5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > )5  1J45  87?J;=5=>  8<?>@B8@0=5  =0  :><?>=5=B0.   !;54  B>20  8715@5B5  ?>;CG5=8OB  D09;  8  87G0:09B5  ?@8:;NG20=5B>  =0  8=AB0;0F8OB0. < p >   &�0  ?J;=>F5==0  @01>B0  A  :;8?1>@40  2  1@0C7J@  F i r e f o x   5  =5>1E>48<>  40  A5  ?>A>G8  @07H8@5=85  70  1@0C7J@. 
 0  704020=5  =0  @07H8@5=85  8715@5B5  1CB>=  " 0?>G=8  8=AB0;0F8OB0" .   @8  ?>O20  =0  AJ>1I5=85  70  B>20,   G5  F i r e f o x   5  1;>:8@0;  70?8B20=5B>  70  8=AB0;0F8O,   8715@5B5  1CB>=  "  07@5H8" ,   A;54  B>20  " =AB0;8@09"   ��;8:=5B5  2J@EC  ?@5?@0B:0B0  4>;C. 
 @0C7J@0  I5  70@548  8=AB0;0F8>==8O  D09;,   A  @07H8@5=85. 
 :>  D09;0  =5  A5  >B2>@8  02B>0<B8G=>,   >B2>@5B5  3>  @JG=>  G@57  42>5=  :;8:  2J@EC  =53>  2  ?@>7>@5F0  70  8<?>@B. 
 =AB0;8@09B5,   A;54209:8  ?>O28;8B5  A5  8=AB@C:F88. 
 >  2@5<5  =0  8=AB0;0F8OB0  <>65B5  40  ?@>25@8B5  4>AB02G8:0  =0  :><?>=5=B0,   :;8:=5B5  2J@EC  >B<5B:0B0  2  3>@=8O  45A5=  J3J;  =0  8=AB0;0F8>==8O  ?@>7>@5F.   #1545B5  A5,   G5  5  ?>A>G5=  4>AB02G8:,   =0  :>9B>  8<0B5  4>25@85!   ��0  ?J;=>F5==0  @01>B0  A  :;8?1>@40  2  1@0C7J@  C h r o m e   5  =5>1E>48<>  40  A5  8=AB0;8@0  @07H8@5=85  70  1@0C7J@0. 
 0  8=AB0;8@0=5  =0  @07H8@5=85B>  =0B8A=5B5  1CB>=  " 0G0;>  =0  8=AB0;0F8OB0" ,   A;54  :>5B>  8715@5B5  1C>=  " + 57?;0B=>  2  >B2>@8;0B0  A5  AB@0=8F0  ( =5>1E>48<>  5  @5AB0@B8@0=5  =0  ?@8;>65=85B>) . 
 0  02B><0B8G=>  8=AB0;8@0=5  =0  @07H8@5=85B>  ?@8  A;5420I>  AB0@B8@0=5  =0  ?@8;>65=85B>  8715@5B5  1CB>=  " B;>68  8=AB0;0F8OB0" . 
 715@5B5  " B:068" ,   70  40  =5  A5  8=AB0;8@0  @07H8@5=85B>  A530.   ��< p > 0  872J@H20=5  =0  459AB285B>  5  =5>1E>48<>  40  A5  8=AB0;8@0  @07H8@5=85  =0  1@0C7J@0  8  % s . < / p > < p > 0  =0G0;>  =08=AB0;0F8OB0  8715@5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > @8  8725640=5  =0  AJ>1I5=85  70  B>20,   G5  F i r e f o x   5  1;>:8@0;  70?8B20=5B>  70  8=AB0;0F8O,   8715@5B5  1CB>=  "  07@5H8" ,   A;54  B>20  " =AB0;8@09" .   @8  8725640=5  =0  70?8B20=5B>  70  8=AB0;0F8O  A5  C25@5B5,   G5  2  :0G5AB2>B>  =0  48AB@81CB>@  =0  :><?>=5=B0  5  ?>A>G5=  4>AB02G8:  =0  @07H8@5=85  70  1@0C7J@0,   =0  :>3>B>  8<0B5  4>25@85! < / p >   
�< p > 0  87?J;=O20=5  =0  459AB28OB0  5  =5>1E>48<>  40  A5  8=AB0;8@0  @07H8@5=85  =0  1@0C7J@0  8  % s . < / p > < p > 0  =0G0;>  =0  8=AB0;0F8OB0  8715@5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > @>3@0<0B0  I5  >B2>@8A  B@0=8F0  A  @07H8@5=8O  2  <03078=0  =0  G o o g l e   C h r o m e .   715@5B5  1CB>=  < b > " + & n b s p ; !"" < / b >   70  8=AB0;0F8O  =0  @07H8@5=85B>.   < / p > < p > !;54  8=AB0;8@0=5  =0  @07H8@5=85B>  5  =5>1E>48<>  40  A5  70B2>@8  AB@0=8F0B0  A  @07H8@5=8O  8  40  A5  8=AB0;8@0  A0<8OB  :><?>=5=B. < / p > < p > 0  B078  F5;  =0?@025B5  ?>;CG5=8OB  D09;  87?J;=8<  8  3>  AB0@B8@09B5  70  87?J;=O20=5. < / p >   2�< p > 0  872J@H20=5  =0  459AB285B>  5  =5>1E>48<>  8=AB0;8@0=5  =0  @07H8@5=85  =0  1@0C7J@0  8  % s . < / p > < p > 0  =0G0;>  =0  8=AB0;0F8OB0  =0B8A=5B5  1CB>=  < b > " @>4J;68" < / b > . < / p > < p > @>3@0<0B0  I5  >B2>@8  AB@0=8F0  A  @07H8@5=8O  2  <03078=0  =0  G o o g l e   C h r o m e .   715@5B5  1CB>=  < b > " + & n b s p ; "" < / b >   70  40  8=AB0;8@0B5  @07H8@5=85B>.   < / p > < p > !;54  8=AB0;8@05  =0  @07H8@5=85B>  5  =5>1E>48<>  40  70B2>@8B5  AB@0=8F0B0  =0  @07H8@5=85B>  8  40  8=AB0;8@0B5  A0<8OB  :><?>=5=B. < / p > < p > 0  B078  F5;  8715@5B5  ?>;CG5=8OB  D09;  ( 2  4>;=0B0  G0AB  =0  ?@>7>@5F0  =0  1@0C7J@0)   8  87G0:09B5  ?@8:;NG20=5  =0  8=AB0;0F8OB0. < / p >   ������N  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>
<p class="title step">Стъпка 1</p>

<p>Натиснете отметка <b><i>Инструменти</i></b>, който се намира в горния десен ъгъл на прозореца.</p>
<p>В менюто изберете пункт <b><i>Settings</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_1_en.png?sysver=<%V8VER%>" /></div>

<p>Ще се отвори меню <b><i>Settings-Basics</i></b>, в което ще се извършват всички бъдещи настройки.</p> 
<p>За връщане към инструкцията изберете меню <b><i>Как да настроим браузъра</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_0_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Стъпка 2</p>

<p>Преминете по препратка <b><i>Show advanced settings...</i></b> в долната част на екрана и я селектирайте.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_2_en.png?sysver=<%V8VER%>" /></div>

<p>Преминете в раздел <b><i>Downloads</i></b> и поставете отметка <b><i>Ask where to save each file before downloading</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_3_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Стъпка 3</p>

<p>След поставяне на отметката, изберете бутон <b><i>Content Settings</i></b>, която е разположена по-горе в раздела <b><i>Privacy</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_4_en.png?sysver=<%V8VER%>" /></div>

<p>В появилият се прозорец <b><i>Content Settings</i></b> преминете към блок <b><i>Pop-Ups</i></b> и поставете отметка <b><i>Allow all sites to show pop-ups</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_5_en.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">Обърнете внимание, че Вашият браузър няма нужда да съхранява промените. 
За да приключите настройката е необходимо само да затворите меню <i>Settings</i>.</p>

</div>
</div>

</body>
</html>
��������������t  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>
<p class="title step">Стъпка 1</p>

<p>Изберете отметка <b><i>Инструменты</i></b>, която се намира в горния десен ъгъл на прозореца.</p>
<p>В менюто изберете <b><i>Настройки</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Ще се отвори меню <b><i>Настройки-Основные</i></b>, в което ще се изпълняват всички бъдещи настройки.</p> 
<p>За да се върнете към инструкциите изберете меню <b><i>Как настроить браузер</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_0_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Стъпка 2</p>

<p>Преминете към препратка <b><i>Показать дополнительные настройки</i></b> в долната част на екрана и я изберете.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_2_ru.png?sysver=<%V8VER%>" /></div>

<p>Преминете към раздел <b><i>Загрузки</i></b> и постаавете отметка <b><i>Запрашивать место для сохранения каждого файла перед загрузкой</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_3_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Стъпка 3</p>

<p>След като поставите отметката, натиснете бутон <b><i>Настройки контента</i></b>, който е разположен горе в раздел <b><i>Личные данные</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_4_ru.png?sysver=<%V8VER%>" /></div>

<p>В появилият се прозорец <b><i>Настройки контента</i></b> преминете към раздел <b><i>Всплывающие окна</i></b> и поставете отметка <b><i>Разрешить открытие всплывающих окон на всех сайтах</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ch_5_ru.png?sysver=<%V8VER%>" /></div>

<p style="background-color:#E4E4E4">Обърнете внимание, че в браузъра не е необходимо да се съхраняват промените. 
За да приключите настройката е достатъчно да затворите меню <i>Настройки</i>.</p>

</div>
</div>

</body>
</html>
��������B  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>

<ul>
    <li>За да се разрешат появяващи се прозорци в приложението, в меню <b>Инструменти (Tools)</b> на браузъра изберете меню <b>Настройки (Options)</b>;<br>
        В отворилият се прозорец преминете към раздел <b>Съдържание (Content)</b>;<br>
        Свалете отметка <b>Блокирай появяващите се прозорци (Block pop-up windows)</b>.</li>
    <li>За да разрешите ръчното преминаване между прозорците в приложението, в адресния ред на браузъра въведете <b>about:config</b>;<br>
        След това в реда на филтъра въведете <code>dom.disable_window_flip</code>;<br>
        Променте стойността на тази настройка на <code>false</code>.</li>
    <li>За да разрешите ръчното използване в параметрите на реда за стартиране на нелатински символи, в адресния ред на браузъра въведете <b>about:config</b>;<br>
        След това в реда на филтъра въведете <code>network.standard-url.encode-query-utf8</code>. Ако настройката не е намерена, въведете <code>browser.fixup.use-utf8</code>;<br>
        Променете стойността на тази настройка на <code>true</code>.</li>
    <li>За ръчното разрешаване за използване на клавиатурата за преминаване между прозорците в приложението, в адресния ред на браузъра въведете <b>about:config</b>;<br>
        След това в реда на филтъра въведете <code>dom.popup_allowed_events</code>;<br>
        Добавете към стойността на това събитие <b>keydown</b>.</li>
    <li>За да настроите ръчно аутентификацията на операционната система, в адресния ред на браузъра въведете <b>about:config</b>;<br>
        След това в страницата с настройки в реда на филтъра въведете наименованието на параметъра;<br>
        Настройката се изпълнява за три параметъра: <code>network.automatic-ntlm-auth.trusted-uris</code>, <code>network.negotiate-auth.delegation-uris</code>, <code>network.negotiate-auth.trusted-uris</code>;<br>
        След това посочете списъка с уеб-услуги, чрез които ще се извършва работа с база на "1С:Предприятие".</li>
</ul>

<p>Настройката е приключена.</p>

</div>
</div>

</body>
</html>
�����������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>
<p class="title step">Стъпка 1</p>

<p>С десен клавиш на мишката изберете в произволна свободна област под адресния ред (на картинката е отбелязано с бледо-червено) 
и в появилото се меню изберете пункт <b><i>Menu Bar</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_0_en.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="e1csys/mngsrv/img_ie_01_en.png?sysver=<%V8VER%>" /></div>

<p>Под адресния ред ще се появи меню. Изберете пункт <b><i>Tools</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_1_en.png?sysver=<%V8VER%>" /></div>

<p>Селектирайте го, ще се отвори меню. Изберете пункт <b><i>Internet Options</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_2_en.png?sysver=<%V8VER%>" /></div>

<p class="title step">Стъпка 2</p>

<p>В появилият се прозорец преминете към таб <b><i>Privacy</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_3_en.png?sysver=<%V8VER%>" /></div>

<p>В средната част на прозореца ще намерите и <b><i>свалете</i></b> отметка <b><i>Turn on Pop-up Blocker</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_4_en.png?sysver=<%V8VER%>" /></div>

<p>След като свалите отметката, изберете бутон <b><i>ОК</i></b>, за да съхраните настройките.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_5_en.png?sysver=<%V8VER%>" /></div>

<p>Настройката е завършена.</p>

<p style='font-size:9pt'>За да скриете менюто под адресния ред, повторете първата част на стъпка 1.</p>

</div>
</div>

</body>
</html>
��g  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>
<p class="title step">Стъпка 1</p>

<p>С десен клавиш на мишката изберете в произволна свободна област под адресния ред (на картинката е отбелязано с бледо-червено) 
и в появилото се меню изберете пункт <b><i>Строка меню</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_0_ru.png?sysver=<%V8VER%>" /></div>

<div><img border=0 src="e1csys/mngsrv/img_ie_01_ru.png?sysver=<%V8VER%>" /></div>

<p>Под адресния ред ще се появи меню. Изберете пункт <b><i>Сервис</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_1_ru.png?sysver=<%V8VER%>" /></div>

<p>Селектирайте го, ще се отвори меню. Изберете пункт <b><i>Свойства обозревателя</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_2_ru.png?sysver=<%V8VER%>" /></div>

<p class="title step">Стъпка 2</p>

<p>В появилият се прозорец преминете към таб <b><i>Конфиденциальность</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_3_ru.png?sysver=<%V8VER%>" /></div>

<p>В средната част на прозореца ще намерите и <b><i>свалете</i></b> отметка <b><i>Включить блокирование всплывающих окон</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_4_ru.png?sysver=<%V8VER%>" /></div>

<p>След като свалите отметката, изберете бутон <b><i>ОК</i></b>, за да съхраните настройките.</p>

<div><img border=0 src="e1csys/mngsrv/img_ie_5_ru.png?sysver=<%V8VER%>" /></div>

<p>Настройката е завършена.</p>

<p style='font-size:9pt'>За да скриете менюто под адресния ред, повторете първата част на стъпка 1.</p>

</div>
</div>

</body>
</html>
������	  ﻿<!DOCTYPE html>
<html class="IWeb" dir="ltr">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Browser setup</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="imagetoolbar" content="no">
        <link href="<%CSS_FILE%>?sysver=<%V8VER%>" rel="stylesheet" type="text/css">
        <style type="text/css">
        .main
        {
            width:720px;
            margin:0 auto;
        }
        .head
        {
            font-size:32pt;
            font-family:Times New Roman;
        }
        .title
        {
            font-size:20pt;
            font-family:Times New Roman;
        }
        .txt
        {
            font-size:14pt;
            font-family:Times New Roman;
        }
        </style>
</head>
<body style="width:100%;height:100%;overflow:hidden;padding:0;margin:0;">
<div style="width:100%;height:100%;overflow-y:auto;overflow-x:hidden;">
<div style="padding:10px;padding-left:30px;padding-right:30px;">
<p><span class="txt"><b>A new window has been blocked, presumably by a pop-up blocker.</b></span></p>
<p><span class="txt">To continue, configure your web browser.<br> To view the instruction, click <b><i>View instruction</i></b>.<br> When you are done, click <b><i>OK</i></b> and restart the application.</span></p>
<div align="center" style="width:100%;height:50px;padding-top:20px;">
    <div style="width:450px;height:30px;position:relative;">
    <button id="okButton" class="webButton" style="left:10px;">View instruction</button>
    <button id="cancelButton" class="webButton" style="left:230px;">OK</button>
    </div>
    </div>
</div>
</div>
<!--@remove+@-->
<script type="text/javascript">
    var CLOSURE_NO_DEPS = true;
    var CLOSURE_BASE_PATH = "scripts/";
</script>
<script type="text/javascript" src="scripts/webtools/libs/closure-library/closure/goog/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/deps.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/base.js?sysver=<%V8VER%>"></script>
<script type="text/javascript" src="scripts/mngsrv/src/res/js/dbgstart_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
<!--@remove-@-->
<script type="text/javascript" src="scripts/mod_browsersettingsinfomain_browsersettingsinfomain.js?sysver=<%V8VER%>"></script>
</body>
</html>
����������  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>

<p>В горният ляв ъгъл намерете меню <b><i>Safari</i></b>, изберете го. в отворилият се прозорец свалете отметка <b><i>Block Pop-Up Windows</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_sf_en.png?sysver=<%V8VER%>" /></div>

<p>Настройката е завършена.</p>

</div>
</div>

</body>
</html>
�����  ﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
        <meta http-equiv="imagetoolbar" content="no">
        <title>Как да настроим браузъра</title>
        <link rel="shortcut icon" href="e1csys/mngsrv/favicon.ico" />
<style type="text/css">
body
{
    width:100%;
    height:100%;
    overflow:hidden;
    padding:0px;
    margin:0px;
}

p
{
    font-size:13pt;
    font-family:Times New Roman;
}

img
{
    margin-bottom:48px;
}

.title
{
    font-size:21pt;
    font-family:Times New Roman;
}

.step
{
    background-color:#99CCFF;
    padding-left:12px;
}
</style>

</head>

<body>
<div style="width:100%;height:100%;overflow:auto;">
<div style="padding:8px;">

<p class="title" style='font-weight:bold'>Настройка на уеб браузъра за 1С:Предприятие</p>

<p>В горният ляв ъгъл намерете меню <b><i>Safari</i></b>, изберете го. в отворилият се прозорец свалете отметка <b><i>Блокировать всплывающие окна</i></b>.</p>

<div><img border=0 src="e1csys/mngsrv/img_sf_ru.png?sysver=<%V8VER%>" /></div>

<p>Настройката е завършена.</p>

</div>
</div>

</body>
</html>
� ��� �#��<�\5�X�<���u�����o3t���^F � � _ � z � ��d?_E2��
���N	�D��~�G`�Y5
V
���r
�	�
z
�0	�$
�
v	��v���	�	0I�	�d
	Z�����	�
��	��
���
�9Ll%E��  ]�wj$Y=��w��(Gj���*A}`�C&���. `S  `  `9 ` `� `� `� `� `%
 `� `� `g ` `� `� `� `j `	 `�% `}# `� `� `X ` `� `O ` `� `� `� `� `
 `D `� `�	 `& `v `>  ` `� ` `�  `i  `� `� `� `� `� `�! `P	 `� `�  `�  `Z `� `y  `�  `  `� ` `	 `_ `0  `)  `� ` ` `� `E  `0 `� `�  `n `� `� ` `q  `@ `
  `� `- `� ` `  `a  `R `�  `� `z `\ `� `x `i `� `d `� `� `L  `�  `> `"  `  `� `x `| `� `7  ` `K `�  `�  `Z `� `} `Z  `� ` `� `< `L ` `�	 `x `G ` `�  `� ` `�  `I
 ` `� `  `! `U `�
 `�
 `' `U `� `< `� `� `� `� `� `� `e `# {  /# +& G) �+ /- 